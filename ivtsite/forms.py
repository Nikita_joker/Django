from django import forms

from ivtsite.models import Feedback, Postmovie


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        exclude = []


class PostmoveForm(forms.ModelForm):
    class Meta:
        model = Postmovie
        exclude = []