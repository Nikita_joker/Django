from django.db import models
from django.utils import timezone


class Contact(models.Model):
    name = models.CharField(max_length=255, verbose_name=u"Имя")
    email = models.EmailField(null=True)
    mobile = models.CharField(max_length=255, verbose_name=u"Мобильный номер", null=True, blank=True)
    work = models.CharField(max_length=255, verbose_name=u"Рабочий номер", null=True, blank=True)


class Postmovie(models.Model):
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=1000)
    text = models.TextField()
    file = models.FileField(upload_to='media', blank=True, null=True)
    create_date = models.DateField(default=timezone.now)
    published_date = models.DateField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title


class Feedback(models.Model):
    name = models.CharField(max_length=255, verbose_name=u"Имя")
    email = models.EmailField(verbose_name=u"Email")
    text = models.TextField(verbose_name=u"Сообщение")
