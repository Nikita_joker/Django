import json
from distutils.command import upload

from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils import timezone
from django.views.generic import TemplateView
from django.contrib import auth, messages

from ivtsite.forms import FeedbackForm, PostmoveForm
from ivtsite.models import Contact, Feedback, Postmovie


class HomeView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = {
            'contacts': Contact.objects.all()
        }
        return context


class FilmsView(TemplateView):
    template_name = "Films.html"


class AnimeView(TemplateView):
    template_name = "Anime.html"


class SeriesView(TemplateView):
    template_name = "Series.html"


class ProfilePage(TemplateView):
    template_name = "registration/profile.html"


class RegisterView(TemplateView):
    template_name = "registration/register.html"

    def dispatch(self, request, *args, **kwargs):
        context = {}
        if request.method == 'POST':
            message = None
            username = request.POST.get("username")
            fullname = request.POST.get("fullname")
            email = request.POST.get("email")
            password = request.POST.get("password")
            password2 = request.POST.get("password2")
            if len(password) >= 8 and password == password2:
                user = User.objects.create_user(username, email, password)
                user.first_name = fullname
                user.save()
                message = "Пользователь успешно зарегистрирован!"
            else:
                message = "Пароль должен содержать более 8 символов"
            context['message'] = message
        return render(request, self.template_name, context)


def logout(request):
    auth.logout(request)
    return redirect('/')


class FeedbackView(TemplateView):
    template_name = "feedback.html"

    def dispatch(self, request, *args, **kwargs):
        context = {}
        form = FeedbackForm()

        if request.method == "POST":
            form = FeedbackForm(request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, u"Спасибо!")
                return redirect(reverse("feedback"))
        context['feedback_form'] = form
        return render(request, self.template_name, context)


class ViewFeedbacksView(TemplateView):
    template_name = "view_feedbacks.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            return redirect(reverse("home"))

        context = {
            'feedbacks': Feedback.objects.all
        }
        return render(request, self.template_name, context)


class Postmovie(TemplateView):
    template_name = "post.html"

    def dispatch(self, request, *args, **kwargs):
        context = {}
        form = PostmoveForm()

        if request.method == "POST":
            form = PostmoveForm(request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, u"Спасибо!")
                return redirect(reverse("profile"))
        context['postmove_form'] = form
        return render(request, self.template_name, context)


class GetMarksAjaxView(TemplateView):
    def dispatch(self, request, *args, **kwargs):
        marks = [
            {
                'student': "Dimitriy",
                'mark': 120
            },
            {
                'student': "Antoniy",
                'mark': 45
            },
            {
                'student': "Nikitiy",
                'mark': 23
            },
            {
                'student': "Ruslaniys",
                'mark': 120
            }
        ]
        return HttpResponse(content = json.dumps(marks), content_type = "application/json")